<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Nilai</title>
	 <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<form class="form-horizontal" method="POST" action="nilai_siswa.php">
<fieldset>

<!-- Form Name -->
<legend>Form Nilai Mahasiswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nama Lengkap</label>  
  <div class="col-md-5">
  <input id="nama" name="nama" type="text" placeholder="Nama Lengkap" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Mata Kuliah</label>
  <div class="col-md-5">
    <select id="matkul" name="matkul" class="form-control">
      <option value="1">Pilih Mata Kuliah</option>
      <option value="Dasar - Dasar Pemrograman">Struktur Dasar Algoritma</option>
      <option value="Struktur Dasar Alghoritma">Pemrograman WEB</option>
      <option value="Pemrograman Berbasis Objek">Basis Data II</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nilai UTS</label>  
  <div class="col-md-4">
  <input id="uts" name="uts" type="text" placeholder="Nilai UTS" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nilai UAS</label>  
  <div class="col-md-4">
  <input id="uas" name="uas" type="text" placeholder="Nilai UAS" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nilai Tugas/Praktikum</label>  
  <div class="col-md-4">
  <input id="tugas" name="tugas" type="text" placeholder="Nilai Tugas" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>

<?php

	//definisikan variable
	/*$proses =$_GET['simpan'];
	$nama =$_GET['nama'];
	$matkul =$_GET['matkul'];
	$uts =$_GET['uts'];
	$uas =$_GET['uas'];
	$tugas =$_GET['tugas'];

	//tampilkan
	echo 'Proses '.$proses;
	echo '<br/> Nama : '.$nama;
	echo '<br/> Mata Kuliah : '.$matkul;
	echo '<br/> UTS : '.$uts;
	echo '<br/> UAS : '.$uas;
	echo '<br/> Tugas/Praktikum : '.$tugas;
	*/
?>

</body>
</html>