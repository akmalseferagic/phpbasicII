<?php
	function kelulusan($_nilai){
		if($_nilai > 55){
			return 'LULUS';
		}else{
			return 'TIDAK LULUS';
		}
	}
	function grade($_grade){
		if($_grade > 0 && $_grade < 35){
			return 'E';
		}elseif($_grade > 36 && $_grade < 55){
			return 'D';
		}elseif($_grade > 56 && $_grade < 69){
			return 'C';
		}elseif($_grade > 70 && $_grade < 84){
			return 'B';
		}elseif($_grade > 85 && $_grade < 100){
			return 'A';
		}else{
			return 'I';
		}
	}

	function predikat($_predikat){
		switch ($_predikat) {
			case 'E':
					return 'Sangat Kurang';
				break;
			case 'D':
					return 'Kurang';
				break;
			case 'C':
					return 'Cukup';
				break;
			case 'B':
					return 'Baik';
				break;
			case 'A':
					return 'Sangat Baik';
				break;
			default:
				return 'Tidak ada';
				break;
		}
	}
?>